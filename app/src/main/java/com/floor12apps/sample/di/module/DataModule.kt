package com.floor12apps.sample.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.floor12apps.auth.AuthorizationManager
import com.floor12apps.sample.data.DataManager
import com.floor12apps.sample.data.local.database.DataBase
import com.floor12apps.sample.data.remote.RemoteApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Module(includes = [(RetrofitModule::class)])
class DataModule {

    @Provides
    @Singleton
    fun provideAuthorizationApi(retrofit: Retrofit): AuthorizationApi {
        return retrofit.create<AuthorizationApi>(AuthorizationApi::class.java)
    }

    @Provides
    @Singleton
    fun provideDataManager(
            restApi: RemoteApi, prefHelper: PrefHelper,
            authorizationManager: AuthorizationManager, db: DataBase
    ): DataManager {
        return DataManager(restApi, prefHelper, authorizationManager, db)
    }

    @Provides
    @Singleton
    fun providePrefHelper(context: Context) = PrefHelper(context)

    @Provides
    @Singleton
    fun provideAuctionApi(retrofit: Retrofit): RemoteApi = retrofit.create(RemoteApi::class.java)

    @Provides
    @Singleton
    fun provideDataBase(context: Context): DataBase = Room.databaseBuilder(
            context, DataBase::class.java,
            "*.db"
    ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
}