package com.floor12apps.sample.di.components

import com.floor12apps.sample.view.menu.MenuActivityPresenter
import com.floor12apps.sample.view.menu.deals.DealActivityPresenter
import com.floor12apps.sample.view.menu.deals.DealDetailFragmentPresenter
import com.floor12apps.sample.view.menu.deals.ExchangesFragmentPresenter
import com.floor12apps.sample.view.menu.skill.general.ServiceDetailFragmentPresenter
import com.floor12apps.sample.view.menu.skill.general.ServicesActivityPresenter
import com.floor12apps.sample.view.menu.skill.general.detail.ServiceDetailActivityPresenter
import com.floor12apps.sample.view.menu.skill.my.detail.MyServiceDetailActivityPresenter
import com.floor12apps.sample.view.menu.skill.my.exchange.ExchangeActivityPresenter
import dagger.Component
import javax.inject.Singleton

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(presenter: MenuActivityPresenter)
    fun inject(presenter: ExchangesFragmentPresenter)
    fun inject(presenter: IncomingOffersFragmentPresenter)
    fun inject(presenter: MyAccountFragmentPresenter)
    fun inject(presenter: OutgoingOffersFragmentPresenter)
    fun inject(presenter: SplashActivityPresenter)
    fun inject(presenter: CreateSkillActivityPresenter)
    fun inject(presenter: DealActivityPresenter)
    fun inject(presenter: DealDetailFragmentPresenter)
    fun inject(presenter: DesireActivityPresenter)
    fun inject(presenter: ServiceDetailFragmentPresenter)
    fun inject(presenter: CreateLocationFilterActivityPresenter)
    fun inject(presenter: CategoryFilterActivityPresenter)
    fun inject(presenter: ProposalMapFragmentPresenter)
    fun inject(presenter: LocationFilterActivityPresenter)
    fun inject(presenter: SelectCategoryActivityPresenter)
    fun inject(presenter: ProposalActivityPresenter)
    fun inject(presenter: ServicesActivityPresenter)
    fun inject(presenter: MyServicesActivityPresenter)
    fun inject(presenter: ServiceDetailActivityPresenter)
    fun inject(presenter: MyServiceDetailActivityPresenter)
    fun inject(presenter: ExchangeActivityPresenter)
}