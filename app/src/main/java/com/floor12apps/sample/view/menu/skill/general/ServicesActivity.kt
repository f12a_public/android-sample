package com.floor12apps.sample.view.menu.skill.general

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.R
import com.floor12apps.sample.adapters.BaseAdapter
import com.floor12apps.sample.base.BaseActivity
import com.floor12apps.sample.data.model.entity.Skill
import com.floor12apps.sample.utils.constants.Constants
import com.floor12apps.sample.utils.helper.ThemeHelper
import com.floor12apps.sample.view.filters.category.CategoryFilterActivity
import com.floor12apps.sample.view.filters.location.LocationFilterActivity
import kotlinx.android.synthetic.main.activity_skill_list.*
import kotlinx.android.synthetic.main.part_list.*
import kotlinx.android.synthetic.main.part_list_empty.*
import kotlinx.android.synthetic.main.part_srl_list.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ServicesActivity : BaseActivity(),
        ServicesActivityView, BaseAdapter.OnItemClickListener<Skill> {

    @InjectPresenter
    lateinit var presenter: ServicesActivityPresenter
    override val layoutId = R.layout.activity_skill_list
    private val adapter by lazy { ServicesAdapter(this, presenter::getCategory) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initToolbar(toolbar)
        initRecyclerView(rvItems)
        ThemeHelper.setSwipeRefreshLayoutStyle(this, srlItems)
        srlItems.setOnRefreshListener { presenter.load() }
        setTitle(getString(R.string.tb_search_services))

        tvHeader?.setText(R.string.tv_nothing_found)
        tvInfo?.setText(R.string.tv_info_change_filter_search)

        fabServiceList?.inflate(R.menu.fab_search_services)

        fabServiceList?.setOnActionSelectedListener { speedDialActionItem ->
            when (speedDialActionItem?.id) {
                R.id.itCategoryFilter -> {
                    gotoSelectCategoriesFilter()
                    false
                }
                R.id.itLocationFilter -> {
                    gotoSelectLocationFilter()
                    false
                }
                else -> false
            }
        }

    }

    override fun initRecyclerView(recyclerView: RecyclerView) {
        super.initRecyclerView(recyclerView)
        recyclerView.adapter = adapter
    }

    override fun showSkills(list: List<Skill>) {
        if (catchAndProcessEmptyList(list)) return
        adapter.setItems(list)
    }

    override fun showProgressDialog() {
        srlItems.isRefreshing = true
    }

    override fun hideProgressDialog() {
        srlItems.isRefreshing = false
    }

    override fun onItemClick(item: Skill, position: Int) {
        super.onItemClick(item, position)
        gotoSkill(item)
    }

    companion object {
        const val TAG = "ServicesActivity"
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, ServicesActivity::class.java)
            return intent
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.load()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.itLocationFilter -> {
                gotoSelectLocationFilter()
                true
            }
            R.id.itCategoriesFilter -> {
                gotoSelectCategoriesFilter()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun gotoSelectCategoriesFilter() {
        startActivityForResult(CategoryFilterActivity.getIntent(this), Constants.RequestCode.SELECT_CATEGORY)
    }

    private fun gotoSelectLocationFilter() {
        startActivityForResult(LocationFilterActivity.getIntent(this), Constants.RequestCode.SELECT_LOCATION)
    }
}