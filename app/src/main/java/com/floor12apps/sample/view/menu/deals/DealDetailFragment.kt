package com.floor12apps.sample.view.menu.deals

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import com.floor12apps.sample.data.model.entity.Deal

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class DealDetailFragment : BaseFragment(), DealDetailFragmentView {

    @InjectPresenter
    lateinit var presenter: DealDetailFragmentPresenter
    override val layoutId = R.layout.fragment_deal_detail_1

    private val dealArg by lazy {
        arguments!!.getParcelable(
                ARG_DEAL) as Deal
    }

    override fun argLoad() {
        super.argLoad()
        presenter.load(dealArg)
    }

    override fun showDeal(deal: Deal) {
        tvName.text = deal.contragent.fullName
        tvEmail.text = deal.contragent.email
        tvPhone.text = deal.contragent.phone

        Glide.with(context!!)
                .applyDefaultRequestOptions(ThemeHelper.getGlideDefaultRequestOptions())
                .load(deal.contragent.picture)
                .into(civUser)

        val detailStatusSide1 =
                when (deal.mySkillStatus) {
                    Deal.SkillStatus.accepted -> getString(R.string.tv_my_service_is_not_provided_yet)
                    Deal.SkillStatus.completed -> getString(R.string.tv_my_service_has_been_provided)
                    Deal.SkillStatus.canceled -> getString(R.string.tv_the_counterparty_no_longer_needs_your_service)
                }
        tvDetailSide1.text = detailStatusSide1

        val detailStatusSide2 =
                when (deal.otherSkillStatus) {
                    Deal.SkillStatus.accepted -> getString(R.string.tv_counterparty_service_is_not_provided_yet)
                    Deal.SkillStatus.completed -> getString(R.string.tv_counterparty_service_has_been_provided)
                    Deal.SkillStatus.canceled -> getString(R.string.tv_i_no_longer_needs_the_counterparty_service)
                }
        tvDetailSide2.text = detailStatusSide2
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnAccept.setOnClickListener { presenter.updateDeal(Deal.SkillStatus.completed) }
        btnCancel.setOnClickListener { presenter.updateDeal(Deal.SkillStatus.canceled) }
    }

    companion object {
        const val TAG = "DealDetailFragment"
        const val ARG_DEAL = "ARG_DEAL"
        fun newInstance(deal: Deal): DealDetailFragment {
            val fragment = DealDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_DEAL, deal)
            fragment.arguments = args
            return fragment
        }
    }

    override fun showControlBtns() {
        btnAccept.visibility = View.VISIBLE
        btnCancel.visibility = View.VISIBLE
    }
}
