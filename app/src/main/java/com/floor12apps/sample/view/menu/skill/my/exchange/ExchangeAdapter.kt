package com.floor12apps.sample.view.menu.skill.my.exchange

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.floor12apps.sample.R
import com.floor12apps.sample.adapters.BaseAdapter
import com.floor12apps.sample.data.model.entity.Skill
import kotlinx.android.synthetic.main.cv_exchange.view.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ExchangeAdapter :
        BaseAdapter<Skill, RecyclerView.ViewHolder>() {

    val base = HashSet<Int>()
    val selected = HashSet<Int>()

    fun setItems(list: List<Skill>, selected: List<Int>) {
        setItems(list)
        base.addAll(selected)
        this.selected.addAll(selected)
    }

    fun setSelected(skill: Skill, isSelected: Boolean) {
        if (isSelected) {
            selected.add(skill.id)
        } else {
            selected.remove(skill.id)
        }
    }

    fun isSelected(skill: Skill) = selected.contains(skill.id)

    override fun setItems(newList: List<Skill>) {
        super.setItems(newList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(
                LayoutInflater.from(parent.context).inflate(R.layout.cv_exchange, parent, false))
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]
        viewHolder.setIsRecyclable(false)
        with(viewHolder as Holder) {
            setIsRecyclable(false)

            cbItem.isChecked = isSelected(item)
            cbItem.text = item.title
            cbItem.setOnClickListener { setSelected(item, cbItem.isChecked) }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val cbItem = view.cbItem!!
    }

    fun getDeletedSkillIds(): List<Int> {
        val list = arrayListOf<Int>()
        base.forEach {
            if (!selected.contains(it)) list.add(it)
        }
        return list
    }

    fun getSelectedSkillIds(): List<Int> {
        val list = arrayListOf<Int>()
        selected.forEach {
            if (!base.contains(it)) list.add(it)
        }
        return list
    }

    fun selectAll() {
        getItems().forEach { selected.add(it.id) }
    }
}