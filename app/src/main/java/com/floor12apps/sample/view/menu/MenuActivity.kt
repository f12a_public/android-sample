package com.floor12apps.sample.view.menu

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.R
import com.floor12apps.sample.base.BaseTabActivity
import com.floor12apps.sample.utils.helper.ThemeHelper
import com.floor12apps.sample.view.menu.desires.OutgoingOffersFragment
import com.floor12apps.sample.view.menu.account.MyAccountFragment
import com.floor12apps.sample.view.menu.deals.ExchangesFragment
import com.floor12apps.sample.view.menu.proposals.IncomingOffersFragment
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_menu.*
import timber.log.Timber

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class MenuActivity : BaseTabActivity(), MenuActivityView {

    @InjectPresenter
    lateinit var presenter: MenuActivityPresenter
    override val layoutId = R.layout.activity_menu
    override val fragmentContainerId = R.id.flContainer
    override val menu get() = bottombar.menu
    override var selectedTabItemId: Int
        get() = bottombar.selectedItemId
        set(value) {
            bottombar.selectedItemId = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initBottomBar()
        syncFirebaseToken()
    }

    override fun initToolbar(toolbar: Toolbar) {
        super.initToolbar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onResume() {
        super.onResume()
        presenter.checkAuthorization()
    }

    override fun initBottomBar() {
        bottombar.setOnNavigationItemSelectedListener {
            selectTab(it.itemId)
            true
        }
        bottombar.selectedItemId = R.id.itAccount
        ThemeHelper.setBottomNavigationViewStyle(bottombar)
    }

    override fun getTabsFragment(tabId: Int): Fragment? {
        return when (tabId) {
            R.id.itMyDesires -> OutgoingOffersFragment.newInstance()
            R.id.itProposals -> IncomingOffersFragment.newInstance()
            R.id.itMyDeals -> ExchangesFragment.newInstance()
            else -> MyAccountFragment.newInstance()
        }
    }

    override fun onTabClicKEvent(tabId: Int) {
        val title = when (tabId) {
            R.id.itMyDesires -> {
                hideFab()
                getString(R.string.mi_outgoing_offers)
            }
            R.id.itProposals -> {
                hideFab()
                getString(R.string.mi_incoming_offers)
            }
            R.id.itMyDeals -> {
                hideFab()
                getString(R.string.mi_exchanges)
            }
            R.id.itAccount -> {
                hideFab()
                getString(R.string.mi_account)
            }
            else -> {
                hideFab()
                ""
            }
        }
        supportActionBar?.title = title
    }

    companion object {
        fun getIntent(context: Context) = Intent(context, MenuActivity::class.java)
    }
}
