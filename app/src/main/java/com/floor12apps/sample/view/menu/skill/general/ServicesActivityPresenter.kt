package com.floor12apps.sample.view.menu.skill.general

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.App
import com.floor12apps.sample.App.Companion.context
import com.floor12apps.sample.R
import com.floor12apps.sample.base.BasePresenter
import com.floor12apps.sample.data.model.entity.Skill
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class ServicesActivityPresenter : BasePresenter<ServicesActivityView>() {

    override fun inject() = App.appComponent.inject(this)

    override fun firstLoad() {
        super.firstLoad()
        load()
    }

    fun load() {
        val unit = { load() }
        viewState.showProgressDialog()

        val filter = dataManager.getSkillsFilter()
        if (catchCategoryIsNotSelected(filter)) return
        if (catchLocationIsNotSelected(filter)) return
        add(
                dataManager.getSkills(filter)
                        .doOnNext { catchNeedRefresh(it, unit) }
                        .map { it.body()!! }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            onSuccessLoad(it)
                        }) {
                            onError(it)
                            viewState.alertDisconnected(context.getString(R.string.ad_failed_to_get_list_services), unit)
                        }
        )
    }

    private fun onSuccessLoad(list: List<Skill>) {
        viewState.hideProgressDialog()
        viewState.showSkills(list)
    }
}

enum class SkillFilter {
    all, hidden, published
}
