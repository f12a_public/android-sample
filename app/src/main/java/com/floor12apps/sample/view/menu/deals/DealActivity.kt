package com.floor12apps.sample.view.menu.deals

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.data.model.entity.Deal
import com.floor12apps.sample.view.menu.skill.general.ServiceDetailFragment

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class DealActivity : BaseTabActivity(),
        DealActivityView {

    @InjectPresenter
    lateinit var presenter: DealActivityPresenter
    override val layoutId = R.layout.activity_deal
    override val fragmentContainerId = R.id.flContainer
    override val menu get() = bottombar.menu
    override var selectedTabItemId: Int
        get() = bottombar.selectedItemId
        set(value) {
            bottombar.selectedItemId = value
        }

    private val argDeal by lazy {
        intent!!.getParcelableExtra<Deal>(
                ARG_DEAL)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initBottomBar()
        setTitle(getString(R.string.mi_deal))
    }

    override fun argLoad() {
        presenter.load(argDeal)
    }

    override fun getTabsFragment(tabId: Int): Fragment? {
        return when (tabId) {
            R.id.itDealDetail -> DealDetailFragment.newInstance(argDeal)
            R.id.itMySkill -> ServiceDetailFragment.newInstance(argDeal.mySkill)
            R.id.itOtherSkill -> ServiceDetailFragment.newInstance(argDeal.otherSkill)
            else -> null
        }
    }

    override fun onTabClicKEvent(tabId: Int) {
        when (tabId) {
            R.id.itDealDetail -> setTitle(getString(R.string.mi_deal))
            R.id.itMySkill -> setTitle(getString(R.string.mi_deal))
            R.id.itOtherSkill -> setTitle(getString(R.string.mi_deal))
        }
    }

    fun showBottomBar() {
        bottombar.visibility = View.VISIBLE
    }

    fun hideBottombar() {
        bottombar.visibility = View.GONE
    }

    override fun initBottomBar() {
        hideBottombar()
        ThemeHelper.setBottomNavigationViewStyle(bottombar)
        bottombar.setOnNavigationItemSelectedListener {
            selectTab(it.itemId)
            true
        }
        bottombar.selectedItemId = R.id.itDealDetail
        showBottomBar()
    }

    override fun initToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun setTitle(title: String) {
        supportActionBar?.apply {
            setTitle(title)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun showDeal(deal: Deal) {

    }

    override fun onBackPressed() {
        if (previousFragment()) super.onBackPressed()
    }

    companion object {
        const val ARG_DEAL = "ARG_DEAL"

        fun getIntent(context: Context, deal: Deal): Intent {
            val intent = Intent(context, DealActivity::class.java)
            intent.putExtra(ARG_DEAL, deal)
            return intent
        }
    }
}
