package com.floor12apps.sample.view.menu.skill.my.exchange

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.App
import com.floor12apps.sample.App.Companion.context
import com.floor12apps.sample.R
import com.floor12apps.sample.base.BasePresenter
import com.floor12apps.sample.data.model.entity.Skill
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class ExchangeActivityPresenter : BasePresenter<ExchangeActivityView>() {

    override fun inject() = App.appComponent.inject(this)

    private var skillArg: Skill? = null

    public fun load(skill: Skill) {
        skillArg = skill
        loadMySkills()
    }

    private fun loadMySkills() {
        val unit = { loadMySkills() }
        val subscription =
                dataManager.getMySkills()
                        .doOnNext { catchNeedRefresh(it, unit) }
                        .map { it.body()!! }
                        .map { it.sortedBy { item -> item.updatedAt } }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            viewState.hideProgressDialog()
                            viewState.showSkills(it, skillArg!!.getSelectedSkills())
                        },
                                {
                                    onError(it)
                                    viewState.alertDisconnected(context.getString(R.string.ad_failed_to_get_list_my_services), unit)
                                })
        add(subscription)
    }

    fun updateSkills(removed: List<Int>, selected: List<Int>) {
        if (removed.isEmpty()) {
            selectSkills(selected)
            return
        }

        val unit = { updateSkills(removed, selected) }
        if (catchDisconnected(unit)) return
        viewState.showProgressDialog()

        add(Flowable.fromIterable(removed)
                .concatMap {
                    return@concatMap dataManager.deleteOffer(getOfferIdBySkillId(it)).map { it.isSuccessful }
                }
                .throttleWithTimeout(15000, TimeUnit.MILLISECONDS)
                .toList(removed.size)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list ->
                    viewState.hideProgressDialog()
                    if (list.firstOrNull { !it } != null) {
                        viewState.alertDisconnected(unit)
                        return@subscribe
                    }

                    selectSkills(selected)
                }, {
                    onThrowable(it)
                    viewState.alertDisconnected(unit)
                }))
    }

    private fun getOfferIdBySkillId(skillId: Int): Int {
        return skillArg?.offers?.firstOrNull { it.initiatorSkillId == skillId }?.id ?: 0
    }

    private fun selectSkills(selected: List<Int>) {
        if (selected.isEmpty()) {
            viewState.toast(context.getString(R.string.toast_offers_updated_successfully))
            viewState.finishWithSuccess()
            return
        }

        val unit = { selectSkills(selected) }
        if (catchDisconnected(unit)) return
        viewState.showProgressDialog()

        add(Flowable.fromIterable(selected)
                .concatMap {
                    return@concatMap dataManager.acceptOffer(skillArg!!.id, it).map { it.isSuccessful }
                }
                .throttleWithTimeout(15000, TimeUnit.MILLISECONDS)
                .toList(selected.size)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list ->
                    viewState.hideProgressDialog()
                    if (list.firstOrNull { !it } != null) {
                        viewState.alertDisconnected(unit)
                        return@subscribe
                    }
                    viewState.toast(context.getString(R.string.toast_offers_updated_successfully))
                    viewState.finishWithSuccess()
                }, {
                    onThrowable(it)
                    viewState.alertDisconnected(unit)
                }))
    }
}
