package com.floor12apps.sample.view.menu.skill.general

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.R
import com.floor12apps.sample.adapters.BaseAdapter
import com.floor12apps.sample.base.BaseMapFragment
import com.floor12apps.sample.data.model.entity.Skill
import com.floor12apps.sample.utils.MapHelper
import com.floor12apps.sample.utils.helper.ThemeHelper
import com.floor12apps.sample.utils.helper._toUrls
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.pawegio.kandroid.runDelayedOnUiThread
import kotlinx.android.synthetic.main.fragment_service_detail.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ServiceDetailFragment : BaseMapFragment(),
        ServiceDetailFragmentView, BaseAdapter.OnItemClickListener<String>, OnMapReadyCallback {

    @InjectPresenter
    lateinit var presenter: ServiceDetailFragmentPresenter
    override val layoutId = R.layout.fragment_service_detail

    private val argSkill by lazy {
        arguments!!.getParcelable<Skill>(
                ARG_SKILL)
    }

    private val imageAdapter = ImageAdapter(this)

    override fun onMapReady(googleMap: GoogleMap) {
        super.onMapReady(googleMap)

        val context = context ?: return
        when (argSkill.locationType) {
            1 -> {
                argSkill.locations.forEach {
                    addMarker(MapHelper._createMarkerOptions(context, it))
                }
            }
            else -> {
                argSkill.locations.forEach {
                    addCircle(MapHelper._createCircleOptions(context, it._toLatLng(), it.distance
                            ?: 1))
                    addMarker(MapHelper._createMarkerOptions(context, it))
                }
            }
        }
        runDelayedOnUiThread(500) { updateCamera() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPhotosBlock()
    }

    private fun initPhotosBlock() {
        ThemeHelper.setDividerToPreviews(rvImages)
        rvImages.layoutManager =
                LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        rvImages.adapter = imageAdapter
    }

    companion object {
        const val TAG = "ServiceDetailFragment"
        const val ARG_SKILL = "ARG_SKILL"
        fun newInstance(skill: Skill): ServiceDetailFragment {
            val fragment = ServiceDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_SKILL, skill)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onItemPhotoClick(item: String, position: Int) {
        val activity = activity ?: return
        gotoShowImage(activity, item)
    }

    override fun argLoad() {
        super.argLoad()
        presenter.load(argSkill)
    }

    override fun show(skill: Skill) {
        tvTitle.text = skill.title
        tvDetail.text = skill.description
        tvCreatedAt._setDateTime(skill.updatedAt)
        showPhotos(skill.files._toUrls())
        tvStatus._setVisibility(skill, ivStatus)
        clCategory._setCategory(presenter.getCategory(skill.categoryId))
    }

    override fun showPhotos(photos: List<String>) {
        if (photos.isEmpty()) {
            rvImages.visibility = View.GONE
            return
        }
        rvImages.visibility = View.VISIBLE
        imageAdapter.setItems(photos)
    }
}

fun TextView._setVisibility(skill: Skill) {
    val text = if (skill.hidden == 1) context.getString(R.string.tv_hidden) else context.getString(R.string.tv_public)
    setText(text)
}

fun TextView._setVisibility(skill: Skill, ivVisibilityIcon: ImageView) {
    val text = if (skill.hidden == 1) context.getString(R.string.tv_hidden) else context.getString(R.string.tv_public)
    val drawable = if (skill.hidden == 1) R.drawable.ic_visibility_off else R.drawable.ic_visibility_on

    setText(text)
    ivVisibilityIcon.setImageResource(drawable)
}

