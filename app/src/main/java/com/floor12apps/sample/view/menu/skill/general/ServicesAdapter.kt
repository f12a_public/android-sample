package com.floor12apps.sample.view.menu.skill.general

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.floor12apps.sample.R
import com.floor12apps.sample.adapters.BaseAdapter
import com.floor12apps.sample.data.model.entity.Skill
import com.pawegio.kandroid.layoutInflater
import kotlinx.android.synthetic.main.cv_service_find.view.*
import kotlinx.android.synthetic.main.part_images.view.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ServicesAdapter(
        onItemClickListener: OnItemClickListener<Skill>,
        private val getCategoryById: (idCategory: Int) -> Category?
) : BaseAdapter<Skill, ServicesAdapter.Holder>(onItemClickListener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(parent.context.layoutInflater!!.inflate(R.layout.cv_service_find, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = list[position]
        with(holder) {
            setIsRecyclable(false)
            setClickListener(itemView, item, position, itemView)
            clCategory._setCategory(getCategoryById(item.categoryId))
            tvTitleSide1.text = item.title
            tvCreatedAt._setDateTime(item.updatedAt)
            val offeredByMe = item.offers?.size
            if (offeredByMe != null) tvOfferedByMe.text = offeredByMe.toString()
            clImages._setImages(llImages, tvNotDisplayed, item.files)
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val clCategory = view.clCategory!!
        val tvTitleSide1 = view.tvTitleSide1!!
        val tvNotDisplayed = view.tvNotDisplayed!!
        val llImages = view.llImages!!
        val clImages = view.clImages!!
        val tvCreatedAt = view.tvCreatedAt!!
        val tvOfferedByMe = view.tvOfferedByMe!!
    }
}


