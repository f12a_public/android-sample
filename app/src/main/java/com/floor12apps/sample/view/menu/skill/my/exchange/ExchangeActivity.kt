package com.floor12apps.sample.view.menu.skill.my.exchange

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.R
import com.floor12apps.sample.adapters.BaseAdapter
import com.floor12apps.sample.base.BaseActivity
import com.floor12apps.sample.data.model.entity.Skill
import kotlinx.android.synthetic.main.activity_category_filter.*
import kotlinx.android.synthetic.main.part_list.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ExchangeActivity : BaseActivity(), ExchangeActivityView,
        BaseAdapter.OnItemClickListener<Skill> {

    @InjectPresenter
    lateinit var presenter: ExchangeActivityPresenter
    override val layoutId = R.layout.activity_category_filter
    private val adapter by lazy { ExchangeAdapter() }

    private val skillArg by lazy {
        intent!!.getParcelableExtra<Skill>(
                ARG_SKILL)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initRecyclerView(rvItems)
    }

    override fun argLoad() {
        super.argLoad()
        presenter.load(skillArg)
    }

    override fun initRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = adapter
    }

    override fun initToolbar(toolbar: Toolbar) {
        super.initToolbar(toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = getString(R.string.ab_select_my_service)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun showSkills(list: List<Skill>, selected: List<Int>) {
        if (catchAndProcessEmptyList(list)) return
        adapter.setItems(list, selected)
    }

    companion object {
        const val ARG_SKILL = "ARG_SKILL"

        const val TAG = "ExchangeActivity"
        fun getIntent(context: Context, skill: Skill): Intent {
            val intent = Intent(context, ExchangeActivity::class.java)
            intent.putExtra(ARG_SKILL, skill)
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_exchange, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item?.itemId) {
            R.id.itApply -> {
                presenter.updateSkills(adapter.getDeletedSkillIds(), adapter.getSelectedSkillIds())
                true
            }
            R.id.itSelectAll -> {
                adapter.selectAll()
                adapter.notifyDataSetChanged()
                true
            }
            R.id.itAddSkill -> {
                gotoCreateSkill()
                true
            }
            R.id.itCancel -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        val alert = getAlertDialog(R.string.empty, R.string.msg_closing_apply,
                DialogInterface.OnClickListener { dialog, which ->
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                })
        alert.show()
    }

}