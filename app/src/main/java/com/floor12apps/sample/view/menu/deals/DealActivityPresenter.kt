package com.floor12apps.sample.view.menu.deals

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.data.model.entity.Deal

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class DealActivityPresenter : BasePresenter<DealActivityView>() {

    lateinit var deal: Deal

    override fun inject() = App.appComponent.inject(this)

    fun load(deal: Deal) {
        this.deal = deal
        viewState.showDeal(deal)
    }
}