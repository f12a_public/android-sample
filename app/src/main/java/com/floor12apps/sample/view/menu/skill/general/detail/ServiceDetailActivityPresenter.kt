package com.floor12apps.sample.view.menu.skill.general.detail

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.App
import com.floor12apps.sample.App.Companion.context
import com.floor12apps.sample.R
import com.floor12apps.sample.base.BasePresenter
import com.floor12apps.sample.data.model.entity.Skill
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class ServiceDetailActivityPresenter : BasePresenter<ServiceDetailActivityView>() {

    var skillArg: Skill? = null

    override fun inject() = App.appComponent.inject(this)

    fun load(skill: Skill) {
        this.skillArg = skill
        val skillId = skill.id

        val unit = { load(skill) }
        viewState.showProgressDialog()
        if (catchDisconnected(unit)) return
        add(
                dataManager.getSkill(skillId)
                        .doOnNext { catchNeedRefresh(it, unit) }
                        .map { it.body()!! }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            viewState.hideProgressDialog()
                            viewState.showSkill(it)
                        }) {
                            onError(it)
                            viewState.alertDisconnected(context.getString(R.string.ad_failed_to_get_service), unit)
                        }
        )
    }
}