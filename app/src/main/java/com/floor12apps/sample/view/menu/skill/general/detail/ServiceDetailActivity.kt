package com.floor12apps.sample.view.menu.skill.general.detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.R
import com.floor12apps.sample.base.BaseActivity
import com.floor12apps.sample.data.model.entity.Skill
import com.floor12apps.sample.utils.Navigator
import com.floor12apps.sample.view.menu.skill.general.ServiceDetailFragment
import kotlinx.android.synthetic.main.activity_desire.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ServiceDetailActivity : BaseActivity(),
        ServiceDetailActivityView {

    @InjectPresenter
    lateinit var presenter: ServiceDetailActivityPresenter
    override val layoutId = R.layout.activity_container

    private val skillArg by lazy {
        intent!!.getParcelableExtra<Skill>(
                ARG_SKILL)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar(toolbar)
        initBottomBar()
        title = getString(R.string.tb_service_description)
    }

    override fun showSkill(skill: Skill) {
        Navigator.replaceFragment(this, R.id.flContainer, ServiceDetailFragment.newInstance(skill))
    }

    override fun argLoad() {
        super.argLoad()
        presenter.load(skillArg)
    }

    companion object {
        const val ARG_SKILL = "ARG_SKILL"

        fun getIntent(context: Context, skill: Skill): Intent {
            val intent = Intent(context, ServiceDetailActivity::class.java)
            intent.putExtra(ARG_SKILL, skill)
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_skill_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.itOfferAnExchange -> {
                gotoExchange(skillArg)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

fun Int._isSuccess(): Boolean {
    return Activity.RESULT_OK == this
}
