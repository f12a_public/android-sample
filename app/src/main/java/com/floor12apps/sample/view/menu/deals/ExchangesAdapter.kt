package com.floor12apps.sample.view.menu.deals

import android.graphics.PorterDuff
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.floor12apps.sample.data.model.entity.Deal
import com.pawegio.kandroid.layoutInflater
import com.werb.pickphotoview.extensions.color

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ExchangesAdapter(
        onItemClickListener: OnItemClickListener<Deal>,
        private val getCategoryById: (idCategory: Int) -> Category?
) : BaseAdapter<Deal, ExchangesAdapter.Holder>(onItemClickListener) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(parent.context.layoutInflater!!.inflate(R.layout.cv_exchanges, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = list[position]
        with(holder) {
            setIsRecyclable(false)
            setClickListener(itemView, item, position, itemView)

            item.mySkill.let {
                tvTitleSide2.text = it.title
                tvStatusSide2._setSkillStatus(item.mySkillStatus, ivStatusSide2)
            }

            item.otherSkill.let {
                clCategory._setCategory(getCategoryById(it.categoryId))
                tvTitleSide1.text = it.title
                tvStatusSide1._setSkillStatus(item.otherSkillStatus, ivStatusSide1)
                clImages._setImages(llImages, tvNotDisplayed, it.files)
            }
            tvCreatedAt._setDateTime(item.updatedAt)
            tvProcess.text = context.getString(R.string.tv_getting_in_exchange)
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val clCategory = view.clCategory!!
        val tvTitleSide1 = view.tvTitleSide1!!
        val tvStatusSide1 = view.tvStatusSide1!!
        val ivStatusSide1 = view.ivStatusSide1!!
        val tvTitleSide2 = view.tvTitleSide2!!
        val tvStatusSide2 = view.tvStatusSide2!!
        val ivStatusSide2 = view.ivStatusSide2!!

        val tvNotDisplayed = view.tvNotDisplayed!!
        val llImages = view.llImages!!
        val clImages = view.clImages!!
        val tvHintCreatedAt = view.tvHintCreatedAt!!
        val tvCreatedAt = view.tvCreatedAt!!
        val tvProcess = view.tvProcess!!
    }
}

fun TextView._setSkillStatus(skillStatus: Deal.SkillStatus, ivStatusSide: ImageView) {
    var statusText: String? = null
    var textColor: Int? = null
    var statusIcon: Int? = null

    when (skillStatus) {
        Deal.SkillStatus.accepted -> {
            statusText = context.getString(R.string.tv_pending)
            textColor = R.color.colorAccepted
            statusIcon = R.drawable.ic_confirmed
        }
        Deal.SkillStatus.completed -> {
            statusText = context.getString(R.string.tv_completed)
            textColor = R.color.colorCompleted
            statusIcon = R.drawable.ic_confirmed
        }
        Deal.SkillStatus.canceled -> {
            statusText = context.getString(R.string.tv_canceled)
            textColor = R.color.colorCanceled
            statusIcon = R.drawable.ic_confirmed
        }
    }

    text = statusText
    setTextColor(ResourcesCompat.getColor(resources, textColor, null))
    ivStatusSide.setImageResource(statusIcon)
    ivStatusSide.setColorFilter(ivStatusSide.context.color(textColor), PorterDuff.Mode.SRC_IN)
}


