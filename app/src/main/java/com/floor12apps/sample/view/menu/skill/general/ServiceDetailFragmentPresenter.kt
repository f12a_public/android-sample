package com.floor12apps.sample.view.menu.skill.general

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.App
import com.floor12apps.sample.base.BasePresenter
import com.floor12apps.sample.data.model.entity.Skill

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class ServiceDetailFragmentPresenter :
        BasePresenter<ServiceDetailFragmentView>() {

    override fun inject() = App.appComponent.inject(this)

    fun load(skill: Skill) {
        viewState.show(skill)
    }
}