package com.floor12apps.sample.view.menu.skill.general

import com.floor12apps.sample.base.BaseMvpView
import com.floor12apps.sample.data.model.entity.Skill

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
interface ServiceDetailFragmentView : BaseMvpView {

    fun showPhotos(photos: List<String>)
    fun show(skill: Skill)
}
