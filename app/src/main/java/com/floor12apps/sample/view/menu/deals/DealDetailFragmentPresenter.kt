package com.floor12apps.sample.view.menu.deals

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.data.model.entity.Deal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class DealDetailFragmentPresenter : BasePresenter<DealDetailFragmentView>() {

    override fun inject() = App.appComponent.inject(this)

    private var dealArg: Deal? = null

    fun load(deal: Deal) {
        this.dealArg = deal
        viewState.showDeal(deal)
    }

    fun updateDeal(status: Deal.SkillStatus) {
        val deal = dealArg ?: return
        val unit = { updateDeal(status) }
        if (catchDisconnected(unit)) return

        viewState.showProgressDialog()
        add(dataManager.updateExchangeDeal(deal.id, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.hideProgressDialog()
                    if (!it.isSuccessful) {
                        viewState.alertDisconnected(unit)
                        return@subscribe
                    }
                }, {
                    onThrowable(it)
                    viewState.alertDisconnected(unit)
                }))
    }
}