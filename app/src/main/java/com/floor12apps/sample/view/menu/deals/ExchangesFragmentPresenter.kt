package com.floor12apps.sample.view.menu.deals

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.data.model.entity.Deal
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class ExchangesFragmentPresenter : BasePresenter<ExchangesFragmentView>() {

    override fun inject() = App.appComponent.inject(this)

    fun load(filter: DealFilter) {
        val unit = { load(filter) }
        viewState.showProgressDialog()
        add(
                dataManager.getDeals()
                        .doOnNext { catchNeedRefresh(it, unit) }
                        .flatMap { Observable.fromIterable(it.body()!!) }
                        .toSortedList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            viewState.hideProgressDialog()
                            viewState.show(it)
                        }) {
                            onError(it)
                            viewState.alertDisconnected(context.getString(R.string.ad_failed_to_get_list_deals), unit)
                        }
        )
    }
}
