package com.floor12apps.sample.view.menu.deals

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.floor12apps.sample.data.model.entity.Deal

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class ExchangesFragment : BaseFragment(),
        ExchangesFragmentView, BaseAdapter.OnItemClickListener<Deal> {

    @InjectPresenter
    lateinit var presenter: ExchangesFragmentPresenter
    override val layoutId = R.layout.fragment_exchanges
    private val adapter by lazy { ExchangesAdapter(this, presenter::getCategory) }
    private var filter = DealFilter.all

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView(rvItems)
        sbgFilter.position = filter.ordinal
        setFilter(filter)
        sbgFilter.refreshDrawableState()
        ThemeHelper.setSwipeRefreshLayoutStyle(context!!, srlItems)
        srlItems.setOnRefreshListener { presenter.load(filter) }
        sbgFilter.setOnClickedButtonListener {
            when (it) {
                0 -> setFilter(DealFilter.all)
                1 -> setFilter(DealFilter.in_process)
                2 -> setFilter(DealFilter.completed)
            }
        }

        tvInfo?.text = ""
    }

    override fun initRecyclerView(recyclerView: RecyclerView) {
        super.initRecyclerView(recyclerView)
        rvItems.adapter = adapter
    }

    override fun show(list: List<Deal>) {
        if (catchAndProcessEmptyList(list)) return
        adapter.setItems(list)
    }

    override fun showProgressDialog() {
        srlItems.isRefreshing = true
    }

    override fun hideProgressDialog() {
        srlItems.isRefreshing = false
    }

    private fun setFilter(filter: DealFilter) {
        this.filter = filter
        presenter.load(filter)
    }

    override fun onItemClick(item: Deal, position: Int) {
        super.onItemClick(item, position)
        gotoDeal(item)
    }

    companion object {
        const val TAG = "ExchangesFragment"
        fun newInstance(): ExchangesFragment {
            val fragment = ExchangesFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.load(filter)
    }
}

enum class DealFilter {
    all, in_process, completed
}
