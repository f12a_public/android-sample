package com.floor12apps.sample.view.menu

import com.arellomobile.mvp.InjectViewState
import com.floor12apps.sample.App
import com.floor12apps.sample.App.Companion.context
import com.floor12apps.sample.R
import com.floor12apps.sample.base.BasePresenter
import com.floor12apps.sample.view.filters.category.getTreeList
import com.floor12apps.sample.view.filters.category.setSelected
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@InjectViewState
class MenuActivityPresenter : BasePresenter<MenuActivityView>() {

    override fun inject() = App.appComponent.inject(this)

    override fun firstLoad() {
        super.firstLoad()
        fetchCategories()
    }

    fun checkAuthorization() {
        if (!dataManager.authorizationManager.isAuthorized) {
            gotoLogin()
            return
        }
    }

    fun syncFirebaseToken(token: String) {
        add(dataManager.sendFcmToken(token)
                .subscribeOn(io.reactivex.schedulers.Schedulers.io())
                .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.i("sendFirebaseToken %s", token)
                }) { Timber.e(it) })
    }

    private fun fetchCategories() {
        val unit = { fetchCategories() }
        add(
                dataManager.fetchCategories()
                        .doOnNext { catchNeedRefresh(it, unit) }
                        .map { it.body()!! }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(::onSuccessFetchCategories) {
                            onError(it)
                            viewState.alertDisconnected(context.getString(R.string.ad_failed_to_get_category), unit)
                        }
        )
    }

    private fun onSuccessFetchCategories(list: List<Category>) {
        val selected = dataManager.getSelectedCategories()
        dataManager.clearCategories()
        dataManager.saveCategories(list.getTreeList().setSelected(selected))
    }
}
