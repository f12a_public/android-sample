package com.floor12apps.sample.data.local.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Dao
interface CategoryDao : BaseDao<Category> {

    @Query("SELECT * FROM categories WHERE id = :id LIMIT 1")
    fun getCategory(id: Int): Category

    @Query("SELECT * FROM categories")
    fun getAll(): List<Category>
}