package com.floor12apps.sample.data

import android.content.Context
import android.net.Uri
import com.floor12apps.sample.data.local.database.DataBase
import com.floor12apps.sample.data.remote.RemoteApi
import com.floor12apps.auth.AuthorizationManager
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Response

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class DataManager(
        private val restApi: RemoteApi, val prefHelper: PrefHelper,
        val authorizationManager: AuthorizationManager, private val db: DataBase
) {

    fun getMySkills() = restApi.getMySkills(authorizationManager.token)

    fun fetchMyDeals(userType: UserType) = restApi.fetchMyDeals(authorizationManager.token, userType.type)

    fun getSkill(skillId: Int) = restApi.getSkill(authorizationManager.token, skillId)

    fun sendDocuments(context: Context, files: List<Uri>): Single<Response<Any>> {
        val parts = ArrayList<MultipartBody.Part>()
        files.forEachIndexed { index, uri ->
            parts.add(prepareFilePart(context, "file[$index]", uri));
        }
        return restApi.sendVerify(authorizationManager.token, parts)
    }
}
