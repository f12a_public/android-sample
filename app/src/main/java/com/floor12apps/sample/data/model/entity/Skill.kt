package com.floor12apps.sample.data.model.entity

import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import com.google.gson.annotations.SerializedName

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
data class Skill(
        @SerializedName("id") @PrimaryKey var id: Int,
        @SerializedName("category_id") var categoryId: Int,
        @SerializedName("user_id") var userId: Int,
        @SerializedName("title") var title: String,
        @SerializedName("description") var description: String,
        @SerializedName("location_type") var locationType: Int,
        @SerializedName("hidden") var hidden: Int,
        @SerializedName("created_at") var createdAt: Int,
        @SerializedName("updated_at") var updatedAt: Int,
        @SerializedName("files") var files: List<File> = arrayListOf(),
        @SerializedName("offers") var offers: List<Offer> = arrayListOf()

) : Comparable<Skill>, Parcelable {

    @SerializedName("locations")
    var locations: List<Location> = arrayListOf()

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.createTypedArrayList(File),
            parcel.createTypedArrayList(Offer)) {
        locations = parcel.createTypedArrayList(Location)
    }

    override fun compareTo(other: Skill): Int {
        return -updatedAt.compareTo(other.updatedAt)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(categoryId)
        parcel.writeInt(userId)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeInt(locationType)
        parcel.writeInt(hidden)
        parcel.writeInt(createdAt)
        parcel.writeInt(updatedAt)
        parcel.writeTypedList(files)
        parcel.writeTypedList(offers)
        parcel.writeTypedList(locations)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Creator<Skill> {
        override fun createFromParcel(parcel: Parcel): Skill {
            return Skill(parcel)
        }

        override fun newArray(size: Int): Array<Skill?> {
            return arrayOfNulls(size)
        }
    }

}