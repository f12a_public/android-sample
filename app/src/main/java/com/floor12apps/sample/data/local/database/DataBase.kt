package com.floor12apps.sample.data.local.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.floor12apps.sample.data.local.database.dao.CategoryDao

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Database(
        entities = [
            (Category::class),
                *
]
,
version = BuildConfig.VERSION_DATABASE,
exportSchema = true
)
@TypeConverters(Converter::class)
abstract class DataBase : RoomDatabase() {

    abstract fun categoryFilterDao(): CategoryDao
}