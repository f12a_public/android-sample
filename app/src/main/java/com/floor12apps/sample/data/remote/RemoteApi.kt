package com.floor12apps.sample.data.remote

import com.floor12apps.sample.data.model.entity.Deal
import com.floor12apps.sample.data.model.entity.Skill
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

private const val BASE = "*"

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
interface RemoteApi {

    @FormUrlEncoded
    @POST("$BASE/skills")
    fun postSkill(
            @Header("authorization") authorizationToken: String,
            @Field("category_id") categoryId: Int,
            @Field("title") title: String,
            @Field("description") description: String,
            @Field("location_type") locationType: Int,
            @Field("hidden") hidden: Int
    ): Observable<Response<Skill>>


    @GET("$BASE/deals/{user_type}")
    fun fetchMyDeals(@Header(
            "token") authorizationToken: String, @Path(
            "user_type") userType: String): Observable<Response<List<Deal>>>

    @GET("$BASE/users/me/skills")
    fun getMySkills(@Header("authorization") authorizationToken: String): Observable<Response<List<Skill>>>

    @GET("$BASE/skills/{skill_id}")
    fun getSkill(@Header("authorization") authorizationToken: String, @Path("skill_id") skillId: Int): Observable<Response<Skill>>

    @POST("$BASE/users/me/verify")
    @Multipart
    fun sendVerify(
            @Header("authorization") authorizationToken: String,
            @Part list: List<MultipartBody.Part>
    ): Single<Response<Any>>

}