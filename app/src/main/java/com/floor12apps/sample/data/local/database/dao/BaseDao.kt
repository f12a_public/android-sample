package com.floor12apps.sample.data.local.database.dao

import android.arch.persistence.room.*

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Dao
interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T)

    @Update()
    fun update(entity: T)

    @Delete
    fun delete(entity: T)
}