package com.floor12apps.sample.data.model.entity

import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import com.floor12apps.auth.data.model.User
import com.google.gson.annotations.SerializedName

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
data class Deal(
        @SerializedName("id") @PrimaryKey var id: Int,
        @SerializedName("my_skill") var mySkill: Skill,
        @SerializedName("other_skill") var otherSkill: Skill,
        @SerializedName("my_skill_status") var mySkillStatus: SkillStatus,
        @SerializedName("other_skill_status") var otherSkillStatus: SkillStatus,
        @SerializedName("created_at") var createdAt: Int,
        @SerializedName("updated_at") var updatedAt: Int,
        @SerializedName("contragent") var contragent: User
) : Comparable<Deal>, Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readParcelable(Skill::class.java.classLoader),
            parcel.readParcelable(Skill::class.java.classLoader),
            Deal.SkillStatus.valueOf(parcel.readString()),
            Deal.SkillStatus.valueOf(parcel.readString()),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readParcelable(User::class.java.classLoader)) {
    }

    override fun compareTo(other: Deal): Int {
        return -updatedAt.compareTo(other.updatedAt)
    }

    enum class SkillStatus {
        accepted,
        completed,
        canceled;
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeParcelable(mySkill, flags)
        parcel.writeParcelable(otherSkill, flags)
        parcel.writeString(mySkillStatus.name)
        parcel.writeString(otherSkillStatus.name)
        parcel.writeInt(createdAt)
        parcel.writeInt(updatedAt)
        parcel.writeParcelable(contragent, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Creator<Deal> {
        override fun createFromParcel(parcel: Parcel): Deal {
            return Deal(parcel)
        }

        override fun newArray(size: Int): Array<Deal?> {
            return arrayOfNulls(size)
        }
    }

}