package com.floor12apps.auth.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
class User() : Parcelable {

    @SerializedName("full_name")
    @Expose
    var fullName: String? = null
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null
    @SerializedName("middle_name")
    @Expose
    var middleName: String? = null
    @SerializedName("last_name")
    @Expose
    var lastName: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("phone")
    @Expose
    var phone: String? = null
    @SerializedName("picture")
    @Expose
    var picture: String? = null
    @SerializedName("picture_url")
    @Expose
    var pictureUrl: String? = null
    @SerializedName("password")
    @Expose
    var password: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("provider_id")
    @Expose
    var providerId: String? = null
    @SerializedName("provider")
    @Expose
    var provider: String? = null
    @SerializedName("gender")
    @Expose
    var gender: String? = null
    @SerializedName("birthdate")
    @Expose
    var birthday: String? = null
    @SerializedName("device_info")
    @Expose
    var deviceInfo: String? = null
    @SerializedName("verify_code")
    @Expose
    var verifyCode: String? = null
    @SerializedName("additional")
    @Expose
    var additionalFields: String? = null


    constructor(parcel: Parcel) : this() {
        fullName = parcel.readString()
        firstName = parcel.readString()
        middleName = parcel.readString()
        lastName = parcel.readString()
        email = parcel.readString()
        phone = parcel.readString()
        picture = parcel.readString()
        pictureUrl = parcel.readString()
        password = parcel.readString()
        updatedAt = parcel.readString()
        createdAt = parcel.readString()
        id = parcel.readString()
        providerId = parcel.readString()
        provider = parcel.readString()
        gender = parcel.readString()
        birthday = parcel.readString()
        deviceInfo = parcel.readString()
        verifyCode = parcel.readString()
        additionalFields = parcel.readString()
    }


    constructor(firstName: String, email: String, phone: String, password: String) : this() {
        this.firstName = firstName
        this.email = email
        this.phone = phone
        this.password = password
    }

    constructor(fullName: String, email: String, phone: String) : this() {
        this.fullName = fullName
        this.email = email
        this.phone = phone
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(fullName)
        parcel.writeString(firstName)
        parcel.writeString(middleName)
        parcel.writeString(lastName)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(picture)
        parcel.writeString(pictureUrl)
        parcel.writeString(password)
        parcel.writeString(updatedAt)
        parcel.writeString(createdAt)
        parcel.writeString(id)
        parcel.writeString(providerId)
        parcel.writeString(provider)
        parcel.writeString(gender)
        parcel.writeString(birthday)
        parcel.writeString(deviceInfo)
        parcel.writeString(verifyCode)
        parcel.writeString(additionalFields)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

}