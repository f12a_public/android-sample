package com.floor12apps.auth.data;

import android.support.annotation.StyleRes;

import com.floor12apps.auth.R;
import com.floor12apps.auth.data.local.PreferencesHelper;
import com.floor12apps.auth.data.model.User;
import com.floor12apps.auth.data.remote.RestApi;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import rx.Observable;

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
public class DataManager {

    private RestApi mRestApi;
    private PreferencesHelper mPreferencesHelper;

    public DataManager(RestApi mRestApi, PreferencesHelper mPreferencesHelper) {
        this.mRestApi = mRestApi;
        this.mPreferencesHelper = mPreferencesHelper;
    }

    public Observable<Response<Token>> login(Credentials credentials) {
        return mRestApi.login(mPreferencesHelper.getLanguage(), credentials);
    }

    public Observable<Response<Token>> register(User user) {
        return mRestApi.register(mPreferencesHelper.getLanguage(), user);
    }

    public Observable<Response<Void>> resetPassword(Credentials credentials) {
        return mRestApi.resetPassword(mPreferencesHelper.getLanguage(), credentials);
    }

    public Observable<Response<Void>> verifyCode(Credentials credentials) {
        return mRestApi.verifyCode(mPreferencesHelper.getLanguage(), credentials);
    }

    public Observable<Response<Void>> changePassword(Credentials credentials) {
        return mRestApi.changePassword(mPreferencesHelper.getLanguage(), credentials);
    }

    public Observable<Response<User>> getUserProfile() {
        return mRestApi.getUserProfile(mPreferencesHelper.getToken());
    }

    public Observable<Response<User>> updateUserProfile(User user) {
        return mRestApi.updateUserProfile(mPreferencesHelper.getToken(),
                mPreferencesHelper.getLanguage(), user);
    }

    public Observable<Response<Token>> refreshToken() {
        return mRestApi.refreshTokens(mPreferencesHelper.getToken());
    }

    public Observable<Response<Token>> updatePassword(String password, String newPassword) {
        return mRestApi.updatePassword(mPreferencesHelper.getToken(), mPreferencesHelper.getLanguage(),
                password, newPassword);
    }

    public Observable<Response<Void>> logout() {
        return mRestApi.logout(mPreferencesHelper.getToken());
    }

    public Observable<Response<Void>> logoutAll() {
        return mRestApi.logoutAll(mPreferencesHelper.getToken());
    }

    public Observable<Response<Token>> loginBySocialNetwork(User user) {
        return mRestApi.loginBySocialNetwork(user);
    }

    public Observable<Response<Void>> updateEmail1(User user) {
        return mRestApi.updateEmail1(mPreferencesHelper.getToken(), mPreferencesHelper.getLanguage(),
                user);
    }

    public Observable<Response<Void>> updateEmail2(User user) {
        return mRestApi.updateEmail2(mPreferencesHelper.getToken(), mPreferencesHelper.getLanguage(),
                user);
    }

    public Observable<Response<Void>> updatePhone1(User user) {
        return mRestApi.updatePhone1(mPreferencesHelper.getToken(), mPreferencesHelper.getLanguage(),
                user);
    }

    public Observable<Response<Void>> updatePhone2(User user) {
        return mRestApi.updatePhone2(mPreferencesHelper.getToken(), mPreferencesHelper.getLanguage(),
                user);
    }

    public Observable<Response<Void>> deleteUserProfile1(String password) {
        return mRestApi.deleteUserProfile1(mPreferencesHelper.getToken(),
                mPreferencesHelper.getLanguage(), password);
    }

    public Observable<Response<Void>> deleteUserProfile2(String verifyCode) {
        return mRestApi.deleteUserProfile2(mPreferencesHelper.getToken(),
                mPreferencesHelper.getLanguage(), verifyCode);
    }

    public Observable<Response<User>> updateUserPicture(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

        return mRestApi.updateUserPicture(mPreferencesHelper.getToken(), body);
    }

    public Observable<Response<List<DeviceInfo>>> getActivityHistory() {
        return mRestApi.getActivityHistory(mPreferencesHelper.getToken());
    }

    public Observable<Response<Void>> addAdditionalField(String key, String value) {
        return mRestApi.addAdditionalField(mPreferencesHelper.getToken(),
                mPreferencesHelper.getLanguage(), key, value);
    }

    public Observable<Response<Void>> updateAdditionalField(String key, String value) {
        return mRestApi.updateAdditionalField(mPreferencesHelper.getToken(),
                mPreferencesHelper.getLanguage(), key, value);
    }

    public String getToken() {
        return mPreferencesHelper.getToken();
    }

    public void setToken(String token) {
        mPreferencesHelper.setToken(token);
    }

    public String getUserId() {
        return mPreferencesHelper.getUserId();
    }

    public void setUserId(String userId) {
        mPreferencesHelper.setUserId(userId);
    }

    public String getUserName() {
        return mPreferencesHelper.getUserName();
    }

    public void setUserName(String userName) {
        mPreferencesHelper.setUserName(userName);
    }

    public String getUserPhone() {
        return mPreferencesHelper.getUserPhone();
    }

    public void setUserPhone(String userPhone) {
        mPreferencesHelper.setUserPhone(userPhone);
    }

    public String getUserEmail() {
        return mPreferencesHelper.getUserEmail();
    }

    public void setUserEmail(String userEmail) {
        mPreferencesHelper.setUserEmail(userEmail);
    }

    public String getUserImage() {
        return mPreferencesHelper.getUserImage();
    }

    public void setUserImage(String userPhotoUrl) {
        mPreferencesHelper.setUserImage(userPhotoUrl);
    }

    public int getUserGenderPosition() {
        return mPreferencesHelper.getUserGenderPosition();
    }

    public void setUserGenderPosition(int position) {
        mPreferencesHelper.setUserGenderPosition(position);
    }

    public String getUserLanguage() {
        return mPreferencesHelper.getLanguage();
    }

    public void setUserLanguge(String languge) {
        mPreferencesHelper.setLanguage(languge);
    }

    public int getUserTheme() {
        return mPreferencesHelper.getStyleResId();
    }

    public void setUserTheme(int syleResId) {
        mPreferencesHelper.setStyleResId(syleResId);
    }

    public String getUserBirhDay() {
        return mPreferencesHelper.getUserBirthDay();
    }

    public void setUserBirthDay(String date) {
        mPreferencesHelper.setUserBirthDay(date);
    }

    public String getApplicationId() {
        return mPreferencesHelper.getApplicationId();
    }

    public void setApplicationId(String applicationId) {
        mPreferencesHelper.setApplicationId(applicationId);
    }

    public int getStyleResId() {
        return mPreferencesHelper.getStyleResId();
    }

    public void setStyleResId(@StyleRes int styleResId) {
        mPreferencesHelper.setStyleResId(styleResId);
    }

    public String getAdditionalField(String key, String defaultValue) {
        return mPreferencesHelper.getAdditionalField(key, defaultValue);
    }

    public void setAdditionalField(String key, String value) {
        mPreferencesHelper.setAdditionalField(key, value);
    }

    public int getAdditionalField(String key, int defaultValue) {
        return mPreferencesHelper.getAdditionalField(key, defaultValue);
    }

    public void setAdditionalField(String key, int value) {
        mPreferencesHelper.setAdditionalField(key, value);
    }

    public long getAdditionalField(String key, long defaultValue) {
        return mPreferencesHelper.getAdditionalField(key, defaultValue);
    }

    public void setAdditionalField(String key, long value) {
        mPreferencesHelper.setAdditionalField(key, value);
    }

    public boolean getAdditionalField(String key, boolean defaultValue) {
        return mPreferencesHelper.getAdditionalField(key, defaultValue);
    }

    public void setAdditionalField(String key, boolean value) {
        mPreferencesHelper.setAdditionalField(key, value);
    }

    public boolean isExistAdditionalField(String key) {
        return mPreferencesHelper.isExistAdditionalField(key);
    }

    public void clear() {
        mPreferencesHelper.clear();
    }

    public Observable<String> getObservableToken() {
        return Observable.just(mPreferencesHelper.getToken());
    }

    public Observable<String> getObservableUserImage() {
        return Observable.just(mPreferencesHelper.getUserImage());
    }

    public Observable<String> getObservableUserName() {
        return Observable.just(mPreferencesHelper.getUserName());
    }

    public Observable<String> getObservableUserEmail() {
        return Observable.just(mPreferencesHelper.getUserEmail());
    }

    public Observable<String> getObservableUserPhone() {
        return Observable.just(mPreferencesHelper.getUserPhone());
    }

    public Observable<Integer> getObservableUserGenderResId() {
        int resId;
        switch (mPreferencesHelper.getUserGenderPosition()) {
            case 0:
                resId = R.string.gender_unknown;
                break;
            case 1:
                resId = R.string.gender_male;
                break;
            case 2:
                resId = R.string.gender_female;
                break;
            default:
                resId = R.string.gender_unknown;
                break;
        }
        return Observable.just(resId);
    }

    public Observable<String> getObservableUserBirthDay() {
        return Observable.just(mPreferencesHelper.getUserBirthDay());
    }

    public void setLanguage(String language) {
        mPreferencesHelper.setLanguage(language);
    }
}
