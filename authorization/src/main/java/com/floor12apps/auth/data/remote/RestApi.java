package com.floor12apps.auth.data.remote;

import com.floor12apps.auth.data.model.User;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;
import rx.Observable;

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
public class RestApi {

    private AuthorizationApi api;

    public RestApi(AuthorizationApi api) {
        this.api = api;
    }

    public Observable<Response<Token>> login(String language, Credentials credentials) {
        return api.login(language, credentials);
    }

    public Observable<Response<Token>> register(String language, User user) {
        return api.register(language, user);
    }

    public Observable<Response<Void>> resetPassword(String language,
                                                    Credentials credentials) {
        return api.resetPassword(language, credentials);
    }

    public Observable<Response<Void>> verifyCode(String language,
                                                 Credentials credentials) {
        return api.verifyCode(language, credentials);
    }

    public Observable<Response<Void>> changePassword(String language,
                                                     Credentials credentials) {
        return api.changePassword(language, credentials);
    }

    public Observable<Response<User>> getUserProfile(String token) {
        return api.getUserProfile(token);
    }

    public Observable<Response<User>> updateUserProfile(String token, String language,
                                                        User user) {
        return api.updateUserProfile(token, language, user);
    }

    public Observable<Response<Token>> refreshTokens(String token) {
        return api.refreshTokens(token);
    }

    public Observable<Response<Token>> updatePassword(String token, String language,
                                                            String password, String newPassword) {
        return api.updatePassword(token, language, password, newPassword);
    }

    public Observable<Response<Void>> logout(String token) {
        return api.logout(token);
    }

    public Observable<Response<Void>> logoutAll(String token) {
        return api.logoutAll(token);
    }

    public Observable<Response<Token>> loginBySocialNetwork(User user) {
        return api.loginBySocialNetwork(user);
    }

    public Observable<Response<Void>> updateEmail1(String token, String language,
                                                   User user) {
        return api.updateEmail1(token, language, user);
    }

    public Observable<Response<Void>> updateEmail2(String token, String language,
                                                   User user) {
        return api.updateEmail2(token, language, user);
    }

    public Observable<Response<Void>> updatePhone1(String token, String language,
                                                   User user) {
        return api.updatePhone1(token, language, user);
    }

    public Observable<Response<Void>> updatePhone2(String token, String language,
                                                   User user) {
        return api.updatePhone2(token, language, user);
    }

    public Observable<Response<Void>> deleteUserProfile1(String token, String language,
                                                         String password) {
        return api.deleteUserProfile1(token, language, password);
    }

    public Observable<Response<Void>> deleteUserProfile2(String token, String language,
                                                         String verifyCode) {
        return api.deleteUserProfile2(token, language, verifyCode);
    }

    public Observable<Response<User>> updateUserPicture(String token, MultipartBody.Part file) {
        return api.updateUserPicture(token, file);
    }

    public Observable<Response<List<DeviceInfo>>> getActivityHistory(String token) {
        return api.getActivityHistory(token);
    }

    public Observable<Response<Void>> addAdditionalField(String token, String language, String key,
                                                         String value) {
        return api.addAdditionalField(token, language, key, value);
    }

    public Observable<Response<Void>> updateAdditionalField(String token, String language, String key,
                                                            String value) {
        return api.updateAdditionalField(token, language, key, key, value);
    }
}
