package com.floor12apps.auth;

import android.app.Application;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.floor12apps.auth.data.DataManager;
import com.floor12apps.auth.di.components.AppComponent;
import com.floor12apps.auth.di.components.DaggerAppComponent;
import com.floor12apps.auth.di.modules.AppModule;
import com.floor12apps.auth.logic.ModuleSignInActivity;
import com.floor12apps.auth.logic.userdetail.activities.UserProfileActivity;
import com.floor12apps.auth.logic.userdetail.fragments.UserProfileFragment;
import com.floor12apps.auth.utils.AuthRxBus;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.floor12apps.auth.utils.Constants.Remote.RESPONSE_NOT_FOUND;
import static com.floor12apps.auth.utils.Constants.Remote.RESPONSE_TOKEN_EXPIRED;
import static com.floor12apps.auth.utils.Constants.Remote.RESPONSE_UNAUTHORIZED;

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
public class AuthorizationManager {

    static AppComponent sAppComponent;
    private static AuthorizationManager instance;
    private static Application sApplication;
    @Inject
    protected AuthRxBus mAuthRxBus;
    @Inject
    protected DataManager mDataManager;

    AuthorizationManager() {
        sAppComponent.inject(this);
    }

    public static AuthorizationManager getInstance() {
        if (instance == null) {
            instance = new AuthorizationManager();
        }
        return instance;
    }

    public static void init(@NonNull Application application, String baseUrl) {
        SocialLoginManager.init(application);
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .retrofitModule(new RetrofitModule(baseUrl))
                .build();
        sApplication = application;
    }

    public <T> Observable<Response<T>> checkToken(Observable<Response<T>> observable) {
        return observable.concatMap(response -> {
            if (response.code() == RESPONSE_TOKEN_EXPIRED) {
                return mDataManager.refreshToken().concatMap(tokenResponse -> {
                    if (tokenResponse.isSuccessful()) {
                        mDataManager.setToken(tokenResponse.body().getToken());
                    } else if (tokenResponse.code() == RESPONSE_UNAUTHORIZED) {
                        Response<T> responseError = Response.error(RESPONSE_UNAUTHORIZED, response.errorBody());
                        return Observable.just(responseError);
                    }
                    return Observable.just(response);
                });
            }
            return Observable.just(response);
        });
    }

    public void clear() {
        mDataManager.clear();
    }

    public String getAdditionalField(String key, String defaultValue) {
        return mDataManager.getAdditionalField(key, defaultValue);
    }

    public long getAdditionalField(String key, long defaultValue) {
        return mDataManager.getAdditionalField(key, defaultValue);
    }

    public boolean getAdditionalField(String key, boolean defaultValue) {
        return mDataManager.getAdditionalField(key, defaultValue);
    }

    public AuthRxBus getAuthRxBus() {
        return mAuthRxBus;
    }

    public String getToken() {
        return mDataManager.getToken();
    }

    public String getUserEmail() {
        return mDataManager.getUserEmail();
    }

    public String getUserId() {
        return mDataManager.getUserId();
    }

    public String getUserName() {
        return mDataManager.getUserName();
    }

    public String getUserPhone() {
        return mDataManager.getUserPhone();
    }

    public String getUserPhoto() {
        return mDataManager.getUserImage();
    }

    public boolean isAuthorized() {
        return !TextUtils.isEmpty(mDataManager.getToken());
    }

    public void justLogout() {
        logout().subscribe(voidResponse -> {
            clear();
            restart();
        }, throwable -> {
            Timber.e(throwable);
        });
    }

    public Observable<Response<Void>> logout() {
        return mDataManager.logout();
    }

    public UserProfileFragment openUserProfileFragment(@IdRes int resId, String language) {
        mDataManager.setLanguage(language);
        return UserProfileFragment.newInstance(resId);
    }

    public Observable<Response<Void>> populateAdditionalField(String key, String value) {
        return mDataManager.updateAdditionalField(key, value)
                .subscribeOn(Schedulers.io())
                .concatMap(response -> {
                    if (response.isSuccessful()) {
                        mDataManager.setAdditionalField(key, value);
                    } else if (response.code() == RESPONSE_NOT_FOUND) {
                        mDataManager.setAdditionalField(key, value);
                        return mDataManager.addAdditionalField(key, value).concatMap(secondResponse -> {
                            if (secondResponse.isSuccessful()) {
                                mDataManager.setAdditionalField(key, value);
                            }
                            return Observable.just(secondResponse);
                        });
                    }
                    return Observable.just(response);
                });
    }

    public Observable<Response<Void>> populateAdditionalField(String key, int value) {
        return populateAdditionalField(key, String.valueOf(value));
    }

    public Observable<Response<Void>> populateAdditionalField(String key, long value) {
        return populateAdditionalField(key, String.valueOf(value));
    }

    public Observable<Response<Void>> populateAdditionalField(String key, boolean value) {
        return populateAdditionalField(key, String.valueOf(value));
    }

    public Observable<Response<Token>> refreshToken() {
        return mDataManager.refreshToken();
    }

    public <T> Observable<Response<T>> refreshToken(Response<T> response) {
        return mDataManager.refreshToken().concatMap(tokenResponse -> {
            if (tokenResponse.isSuccessful()) {
                mDataManager.setToken(tokenResponse.body().getToken());
            } else if (tokenResponse.code() == RESPONSE_UNAUTHORIZED) {
                Response<T> responseError = Response.error(RESPONSE_UNAUTHORIZED, response.errorBody());
                return Observable.just(responseError);
            }
            return Observable.just(response);
        });
    }

    public void restart() {
        Intent i =
                sApplication.getPackageManager().getLaunchIntentForPackage(sApplication.getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sApplication.startActivity(i);
    }

    public void saveToken(String token) {
        mDataManager.setToken(token);
    }

    public void saveUserEmail(String userEmail) {
        mDataManager.setUserEmail(userEmail);
    }

    public void saveUserId(String userId) {
        mDataManager.setUserId(userId);
    }

    public void saveUserName(String userName) {
        mDataManager.setUserName(userName);
    }

    public void saveUserPhone(String userPhone) {
        mDataManager.setUserPhone(userPhone);
    }

    public void saveUserPhoto(String userPhoto) {
        mDataManager.setUserImage(userPhoto);
    }

    public void setAdditionalField(String key, String value) {
        mDataManager.setAdditionalField(key, value);
    }

    public void setAdditionalField(String key, int value) {
        mDataManager.setAdditionalField(key, value);
    }

    public void setAdditionalField(String key, long value) {
        mDataManager.setAdditionalField(key, value);
    }

    public void setAdditionalField(String key, boolean value) {
        mDataManager.setAdditionalField(key, value);
    }

    public void setApplicationIdToHeader(String applicationId) {
        mDataManager.setApplicationId(applicationId);
    }

    public void startSignInActivity(AppCompatActivity activity) {
        activity.startActivity(new Intent(activity, ModuleSignInActivity.class));
    }

    public void startSignInActivity(AppCompatActivity activity, @StyleRes int resIdStyle,
                                    String language) {
        mDataManager.setLanguage(language);
        mDataManager.setStyleResId(resIdStyle);
        activity.startActivity(new Intent(activity, ModuleSignInActivity.class));
    }

    public void startUserProfileActivity(AppCompatActivity activity) {
        activity.startActivity(new Intent(activity, UserProfileActivity.class));
    }

    public void startUserProfileActivity(FragmentActivity activity) {
        activity.startActivity(new Intent(activity, UserProfileActivity.class));
    }

    public void startUserProfileActivity(AppCompatActivity activity, @StyleRes int resIdStyle) {
        mDataManager.setStyleResId(resIdStyle);
        activity.startActivity(new Intent(activity, UserProfileActivity.class));
    }
}


