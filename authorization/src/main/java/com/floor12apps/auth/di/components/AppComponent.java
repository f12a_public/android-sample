package com.floor12apps.auth.di.components;

import com.floor12apps.auth.AuthorizationManager;
import com.floor12apps.auth.di.modules.AppModule;
import com.floor12apps.auth.di.scopes.AppScope;
import com.floor12apps.auth.logic.ModuleSignInActivity;
import com.floor12apps.auth.logic.ModuleSignInPresenter;
import com.floor12apps.auth.logic.recoverypassword.activities.RecoveryPasswordActivity;
import com.floor12apps.auth.logic.recoverypassword.presenters.ModuleRecoveryPasswordPresenter;
import com.floor12apps.auth.logic.recoverypassword.presenters.RecoveryChangePasswordPresenter;
import com.floor12apps.auth.logic.recoverypassword.presenters.RecoveryPasswordActivityPresenter;
import com.floor12apps.auth.logic.registration.activities.ModuleRegistrationActivity;
import com.floor12apps.auth.logic.registration.presenters.ModuleRegistrationPresenter;
import com.floor12apps.auth.logic.userdetail.presenters.ActivityHistoryPresenter;
import com.floor12apps.auth.logic.userdetail.presenters.ChangePasswordPresenter;
import com.floor12apps.auth.logic.userdetail.presenters.ChangeUserInfoFragmentPresenter;
import com.floor12apps.auth.logic.userdetail.presenters.DeleteAccountFragmentPresenter;
import com.floor12apps.auth.logic.userdetail.presenters.UserProfileActivityPresenter;
import com.floor12apps.auth.logic.userdetail.presenters.UserProfileFragmentPresenter;

import dagger.Component;

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@AppScope
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(ModuleSignInPresenter presenter);

    void inject(ModuleRegistrationPresenter presenter);

    void inject(ModuleRecoveryPasswordPresenter presenter);

    void inject(UserProfileFragmentPresenter presenter);

    void inject(BaseFragment presenter);

    void inject(ChangeUserInfoFragmentPresenter presenter);

    void inject(ChangePasswordPresenter presenter);

    void inject(DeleteAccountFragmentPresenter presenter);

    void inject(ActivityHistoryPresenter presenter);

    void inject(RecoveryPasswordActivityPresenter presenter);

    void inject(RecoveryChangePasswordPresenter presenter);

    void inject(UserProfileActivityPresenter presenter);

    void inject(BaseActivity activity);

    void inject(ModuleSignInActivity activity);

    void inject(ModuleRegistrationActivity activity);

    void inject(RecoveryPasswordActivity activity);

    void inject(AuthorizationManager manager);
}
