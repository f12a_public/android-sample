package com.floor12apps.auth.di.modules;

import android.content.Context;

import com.floor12apps.auth.data.DataManager;
import com.floor12apps.auth.data.local.PreferencesHelper;
import com.floor12apps.auth.data.remote.RestApi;
import com.floor12apps.auth.di.scopes.AppScope;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Module(includes = {RetrofitModule.class})
public class DataModule {

    @Provides
    @AppScope
    AuthorizationApi provideAuthorizationApi(Retrofit retrofit) {
        return retrofit.create(AuthorizationApi.class);
    }

    @Provides
    @AppScope
    RestApi provideRestApi(AuthorizationApi api) {
        return new RestApi(api);
    }

    @Provides
    @AppScope
    DataManager provideDataManager(RestApi restApi,
                                   PreferencesHelper preferencesHelper) {
        return new DataManager(restApi, preferencesHelper);
    }

    @Provides
    @AppScope
    PreferencesHelper providePreferencesHelper(Context context) {
        return new PreferencesHelper(context);
    }
}
