package com.floor12apps.auth.di.modules;

import android.app.Application;
import android.content.Context;

import com.floor12apps.auth.base.Navigator;
import com.floor12apps.auth.di.scopes.AppScope;
import com.floor12apps.auth.utils.AuthRxBus;

import dagger.Module;
import dagger.Provides;

/*
 * Данный файл является примером исходного кода из реального проекта компании Floor12Apps.
 * Все права защищены Floor12Apps.
 * Данный код только для ознакомления. После ознакомления его необходимо удалить.
 *
 * Файл не является полным.
 * Некоторые ключи\строки были заменены на *.
 * Все комментарии были удалены.
 */
@Module(includes = {DataModule.class})
public class AppModule {

    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @AppScope
    Context provideAppContext() {
        return mApplication;
    }

    @Provides
    @AppScope
    public Navigator provideNavigator() {
        return new Navigator();
    }

    @Provides
    @AppScope
    public AuthRxBus provideRxBus() {
        return new AuthRxBus();
    }
}
